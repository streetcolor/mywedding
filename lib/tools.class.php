<?php
namespace MyWedding\Lib;

class Tools
{
    const SALT_KEY = '5D8mFmI6LCWn1h9Wd9ckJZZLBuFuSFAsm';

    public static function encrypt($string)
    {
        //return $string; 
        return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(self::SALT_KEY), serialize($string), MCRYPT_MODE_CBC, md5(md5(self::SALT_KEY)))), '+/=', '-_,');
    }

    public static function decrypt($encrypted)
    {
        //return $string;
        return unserialize(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(self::SALT_KEY), base64_decode(strtr($encrypted, '-_,', '+/=')), MCRYPT_MODE_CBC, md5(md5(self::SALT_KEY))), "\0"));
    }
    
}
