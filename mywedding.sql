-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Dim 17 Janvier 2016 à 12:09
-- Version du serveur :  5.5.41-0ubuntu0.14.04.1
-- Version de PHP :  5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mywedding`
--

-- --------------------------------------------------------

--
-- Structure de la table `Couple`
--

CREATE TABLE `Couple` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wife` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `husband` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_event` date NOT NULL,
  `guestNumber` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Couple`
--

INSERT INTO `Couple` (`id`, `user_id`, `document_id`, `address`, `wife`, `husband`, `date_event`, `guestNumber`, `budget`, `date_created`, `date_updated`) VALUES
(1, 1, 3, NULL, '0', '0', '2000-01-01', 0, 0, '2015-08-03 20:12:40', '2015-08-03 20:12:40'),
(2, 2, 4, NULL, '0', '0', '2000-01-01', 0, 0, '2015-08-03 20:13:08', '2015-08-03 20:13:08'),
(3, 3, 5, NULL, '0', '0', '2000-01-01', 0, 0, '2015-08-13 09:10:48', '2015-08-13 09:10:48');

-- --------------------------------------------------------

--
-- Structure de la table `Document`
--

CREATE TABLE `Document` (
  `id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Document`
--

INSERT INTO `Document` (`id`, `url`, `alt`) VALUES
(3, 'jpeg', 'kiki-hpg'),
(4, 'jpeg', '07.jpg'),
(5, 'png', 'Capture d’écran 2015-08-06 à 19.05.46.png');

-- --------------------------------------------------------

--
-- Structure de la table `Guest`
--

CREATE TABLE `Guest` (
  `id` int(11) NOT NULL,
  `guest_relation_id` int(11) DEFAULT NULL,
  `couple_id` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1',
  `isDiner` tinyint(1) NOT NULL DEFAULT '1',
  `isNotified` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Guest`
--

INSERT INTO `Guest` (`id`, `guest_relation_id`, `couple_id`, `id_status`, `isDiner`, `isNotified`, `email`, `firstname`, `lastname`, `address`) VALUES
(1, NULL, 1, 2, 0, 0, 'jean.bonardot@gmail.com', 'Jean', 'Bonradot', '8 rue paul gimont'),
(2, 3, 1, 1, 0, 0, 'sebaste.tet@yeye.fr', NULL, 'tetard', NULL),
(3, NULL, 1, 1, 1, 0, 'sebaszebkjet@yeye.fr', NULL, NULL, NULL),
(4, NULL, 1, 1, 1, 0, 'sebaeet@yeyeefr', NULL, NULL, NULL),
(5, NULL, 2, 1, 1, 0, 'bonardotjean@hotmail.com', NULL, NULL, NULL),
(6, NULL, 2, 1, 1, 0, 'gthtkhegjker@gmail.com', NULL, NULL, NULL),
(7, NULL, 2, 1, 1, 0, 'shdjhsshbs@restour.com', NULL, NULL, NULL),
(8, NULL, 2, 1, 1, 0, 'sjkdfdshfsd@filipop.com', NULL, NULL, NULL),
(9, 4, 3, 1, 0, 0, 'sebastien.tetard@yahoo.fr', 'Sébastien', 'Tetard', '84 rue Amiral Mouchez');

-- --------------------------------------------------------

--
-- Structure de la table `GuestRelation`
--

CREATE TABLE `GuestRelation` (
  `id` int(11) NOT NULL,
  `couple_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `GuestRelation`
--

INSERT INTO `GuestRelation` (`id`, `couple_id`, `name`) VALUES
(4, 3, 'Collegue'),
(3, 1, 'Test ');

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `User`
--

INSERT INTO `User` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `google_id`, `facebook_id`) VALUES
(1, 'sebastien.tetard@yahoo.fr', 'sebastien.tetard@yahoo.fr', 'sebastien.tetard@yahoo.fr', 'sebastien.tetard@yahoo.fr', 1, 'ans6sihu33ksgkckwkg4os8o8ocoowk', 'Wk4wAXWzVOEy3XM5IBGHm2vcByz/i7IBAPAEyHywHqT9At+gbbWMs7Ng4fkvm1U/xizQBXmKNY5qQrNzVBU2rA==', '2015-08-12 22:58:06', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_COUPLE";}', 0, NULL, NULL, NULL),
(2, 'bonardotjean@hotmail.com', 'bonardotjean@hotmail.com', 'bonardotjean@hotmail.com', 'bonardotjean@hotmail.com', 1, '8m7cql5qpvwo8os44g0kosck08k4cs0', 'l6mCgAOVyMktm+ar2roZMB53Opa4Jz1RD0LxKMOGGuj6zBSU/J7B/BMMmin3OjHeuxu6X0VvsOxmAlJ2obTDcg==', '2015-08-06 18:01:35', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_COUPLE";}', 0, NULL, NULL, NULL),
(3, 'amaury.lanthier@artsper.com', 'amaury.lanthier@artsper.com', 'amaury.lanthier@artsper.com', 'amaury.lanthier@artsper.com', 1, '3hussbwtu46c8og04s88s0w0wc40ck4', 'KMkVo1flQXq87ZgnTX24jY7h7TW6PRi6RLF4LfytBnojcE7Ar7cHchUv0c0COCa3xUJQ8VMdfxYwTnjVn6ZApg==', '2015-08-13 09:10:49', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_COUPLE";}', 0, NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Couple`
--
ALTER TABLE `Couple`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_DFECB07FA76ED395` (`user_id`),
  ADD UNIQUE KEY `UNIQ_DFECB07FC33F7837` (`document_id`);

--
-- Index pour la table `Document`
--
ALTER TABLE `Document`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Guest`
--
ALTER TABLE `Guest`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`email`,`couple_id`),
  ADD KEY `IDX_6D76B531B5632AD9` (`guest_relation_id`),
  ADD KEY `IDX_6D76B531F66468CA` (`couple_id`);

--
-- Index pour la table `GuestRelation`
--
ALTER TABLE `GuestRelation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`name`,`couple_id`),
  ADD KEY `IDX_1C0C4BDBF66468CA` (`couple_id`);

--
-- Index pour la table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2DA1797792FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_2DA17977A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_2DA1797776F5C865` (`google_id`),
  ADD UNIQUE KEY `UNIQ_2DA179779BE8FD98` (`facebook_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Couple`
--
ALTER TABLE `Couple`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `Document`
--
ALTER TABLE `Document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `Guest`
--
ALTER TABLE `Guest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `GuestRelation`
--
ALTER TABLE `GuestRelation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Couple`
--
ALTER TABLE `Couple`
  ADD CONSTRAINT `FK_DFECB07FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  ADD CONSTRAINT `FK_DFECB07FC33F7837` FOREIGN KEY (`document_id`) REFERENCES `Document` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `Guest`
--
ALTER TABLE `Guest`
  ADD CONSTRAINT `FK_6D76B531B5632AD9` FOREIGN KEY (`guest_relation_id`) REFERENCES `GuestRelation` (`id`),
  ADD CONSTRAINT `FK_6D76B531F66468CA` FOREIGN KEY (`couple_id`) REFERENCES `Couple` (`id`);

--
-- Contraintes pour la table `GuestRelation`
--
ALTER TABLE `GuestRelation`
  ADD CONSTRAINT `FK_1C0C4BDBF66468CA` FOREIGN KEY (`couple_id`) REFERENCES `Couple` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
