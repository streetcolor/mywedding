
$(document).ready(function (e) {

	/* center modal */
	function centerModals($element) {
	  var $modals;
	  if ($element.length) {
	    $modals = $element;
	  } else {
	    $modals = $('.modal-vcenter:visible');
	  }
	  $modals.each( function(i) {
	    var $clone = $(this).clone().css('display', 'block').appendTo('body');
	    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
	    top = top > 0 ? top : 0;
	    $clone.remove();
	    $(this).find('.modal-content').css("margin-top", top);
	  });
	}


	$("#budget").on('click', '.ico40X40.add', function(e){

		$.get( "/web/app_dev.php/couple/budget/add", function( data ) {
			$('body').append(data);
			$('#myModal').modal();
			
			$('#myModal').on('shown.bs.modal', function(e) {
				centerModals($(this));
			});
			$(window).on('resize', centerModals);
		});


	})
	  
});

