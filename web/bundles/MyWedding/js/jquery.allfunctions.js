
$(document).ready(function (e) {



$.browser = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));


$("#add").on("submit", "#edit_guest", function(e) {
	e.preventDefault();
	$this = $('#edit_guest');
	$target = $('#target_edit').val() ? $('#target_edit').val() : $('#target_add').val();

	$.ajax({
	  method: "POST",
	  url: $target,
	  data:$this.serialize(),
	  dataType :'JSON',
	  success : function(response){
	  	$(".error").empty();
	  	if(response.status=="error"){
	  		$.each(response.content, function(i, item){
		  		$(".error_"+i).html(item);
		  	})
	  	}
	  	else{
	  		table.draw();
	  		$this[0].reset();
  			$('#guestTab a').eq(0).tab('show');

	  	}
	  	
	  }
	})
});

$('#guestTab').on('click', 'li a', function(e){

	$this = $(this);
	if($this.attr('href')=="#add"){
		$("#target_edit").val('');
		$('#edit_guest .form-control').val('');
	}

})

var $columns = [];
if($.browser){

	$columns = [
            {
                "targets": [ 0 ],
                "visible": false,
            },
            {
                "targets": [ 1 ],
                "visible": false
            },
             {
                "targets": [ 2 ],
                "width": '60%'
            },
            {
                "targets": [ 3 ],
                "visible": false
            },
           
         
            {
                "targets": [ 5 ],
                "visible": false
            },
            {
                "targets": [ 6 ],
                "visible": false
            }
        ];
	
}

var table = $('#guest_table').DataTable( {
		"processing": true,
		"serverSide": true,
		"responsive":true,
		"dom": 'f<"clearfix">',
		 "columnDefs": $columns,
		  "iDisplayLength": 10000,
		"ajax": {
			"url": "/web/app_dev.php/couple/guest/ajax",
			"type": "POST",
			"data": function ( d ) {
				//d.id_group = $('.group select').val();
				// d.custom = $('#myInput').val();
				// etc
			}
		},
		 "autoWidth": false,
		"fnInitComplete": function(oSettings, json) {
	      $("#guest_table_filter input").addClass('form-control ico60X60');
	    },
	    "language": {
	        "searchPlaceholder": "Recherchez un invité par leur nom, email, adresse, relation, présence...",
		    "sSearch":         "",

	    },
	
		
	});

$("#main").on('click', '.fa-pencil', function(e){
	e.preventDefault();
	$this = $(this);
	$('#guestTab a').eq(1).tab('show');

	$.ajax({
		  method: "GET",
		  url: $this.data('target'),
		  dataType :'JSON',
		  success : function(response){
		  
			  $.each(response, function(i, item){
			  	console.log("guest_creation_"+i+ " => "+item);
			  	$('#guest_creation_'+i).val(item);
			  })

			  $('#target_edit').val($this.data('target'));
			  $('.selectpicker').selectpicker('refresh');
		  }
		})

})


	$(document).on('click', "#addRelation", function(){

			
			$html ='		<div class="form-group padding-top-10">';
			$html +='		    <label class="col-sm-4 control-label hidden-xs required" for="other">Autre</label>';
			$html +='	        <div class="col-sm-8">';
			$html +='			    <input type="text" id="other" name="guestRelation" required="required" class="form-control" placeholder="AUtre" value="">';
			$html +='			</div>';
			$html +='		</div>';

			$("#other").html($html);

	})

    $(document).on("click", "#connect", function() {
        $( "#form-connexion" ).slideToggle( "slow", function() {
        // Animation complete.
        });
    });
    
    $(".date").datepicker();
/*
    $('input[type=file]').change(function() { 
	    // select the form and submit
	    $('#upload-avatar form').submit(); 
	});

	
*/

$("#uploadFile").click(function() {
	    $('input[type=file]').click();
	})

function progressHandlingFunction(e){
    if(e.lengthComputable){
    	 $total  = e.total;
    	 $percent = (e.loaded*100)/$total;
        $('#progress-bar').css({'width': $percent+"%"});
    }
}
$('.ajax_file').on('change', function(){
    // On récupère les élément du DOM
    var $this = $(this);

    // On récupère le fichier
    var file = $this[0].files[0];

    // On supprime le bouton et on affiche la barre de progression

    // On crée un FormData, c'est ce qu'on va envoyer au serveur
    var data = new FormData();
    data.append('file', file);

    // On envoie la requête AJAX
    $.ajax({
	    type: 'POST',
	    xhr: function() {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload){ // Check if upload property exists
                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
            }
            return myXhr;
        },
	    // On récupère l'url dans l'attribut data-target
	    url: $this.data('target'),
	    // On passe le FormData en paramètre
	    data: data,
	    // On ne précise pas le contentType pour l'upload de fichier
	    contentType: false,
	    // Comme on passe directement un FormData on désactive le processData
	    processData: false,
        beforeSend: function(response){
	    	$("#progress-bar").show();
	    },

	    success: function(response){
	            $('#progress-bar').fadeOut();

        $('#progress-bar').css({'width': 0});
	       $("#uploadFile").attr('src', response.content);
	    },
	    error: function(response){
	        // On indique qu'il y a une erreur dans la barre de chargement
	       
	    }
     
	});
});



/*

       var addresspickerMap = $( "#place_input" ).addresspicker({
			regionBias: "fr",
			language: "fr",
			updateCallback: function (geocodeResult, parsedGeocodeResult){
			console.log(parsedGeocodeResult);
				$("#mywedding_couple_edition_locality").val(parsedGeocodeResult.locality);
				$("#mywedding_couple_edition_country").val(parsedGeocodeResult.country);
				$("#mywedding_couple_edition_lat").val(parsedGeocodeResult.lat);
				$("#mywedding_couple_edition_lng").val(parsedGeocodeResult.lng);

			}
	     
	    });
	    
	    */
	  
});

