<?php
namespace MyWedding\TemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class TemplateController extends Controller{
    
    private $environement = 'front'; 
    
    public function __construct(){
       
       
        
    }



    public function headerAction(Request $request){
      
        
         if ($this->get('security.context')->isGranted('ROLE_COUPLE')) {

            $this->environement = "couple";

        }
        elseif ($this->get('security.context')->isGranted('ROLE_PROVIDER')) {

            $this->environement = "provider";

        }
        else{
            $this->environement = "front";

        }
        
        
        return $this->render('MyWeddingTemplateBundle:Template:'.$this->environement.'.header.html.twig');
    
    }

    public function sideAction(Request $request){
           
        if ($this->get('security.context')->isGranted('ROLE_COUPLE')) {

            $this->environement = "couple";

        }
        
        
        return $this->render('MyWeddingTemplateBundle:Template:'.$this->environement.'.side.html.twig');
    
    }

    
    public function footerAction(Request $request){
           
         if ($this->get('security.context')->isGranted('ROLE_COUPLE')) {

            $this->environement = "couple";

        }
        elseif ($this->get('security.context')->isGranted('ROLE_PROVIDER')) {

            $this->environement = "provider";

        }
        else{
            $this->environement = "front";

        }
        
        
        return $this->render('MyWeddingTemplateBundle:Template:'.$this->environement.'.footer.html.twig');
    
    }
  
} 