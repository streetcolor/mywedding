<?php

namespace MyWedding\GuestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use MyWedding\GuestBundle\Entity\Guest;
use MyWedding\GuestBundle\Entity\GuestRelation;
use MyWedding\GuestBundle\Form\Type\GuestType;
use MyWedding\GuestBundle\Form\Type\MessageType;
use Hip\MandrillBundle\Message;
use MyWedding\Lib\Tools;


class MessageController extends Controller
{
  

    public function validationAction($guest=false, $type=false, $couple=false, Request $request){

        $em = $this->getDoctrine()->getManager();

        $message = "Une erreur est survenue" ; 


        if ($guest) {

            $guest = $em
                ->getRepository('MyWeddingGuestBundle:Guest')
                ->findOneById($guest);

            if($couple==$guest->getCouple()->getId()){


                $guest
                    ->setIdStatus($type);

                $em->flush();

                $message = "Confirmation validée" ; 

            }
        }

        return $this->render('MyWeddingGuestBundle:Guest:confirmation.html.twig', array('message'=>$message));
  
    }

    public function messageAction($id=false, Request $request){

        if($this->get('security.context')->isGranted('ROLE_COUPLE')){
            $guests = [];
            
            $couple = $this->get('mywedding.init.couple')->getCouple();

            $formMessage = $this->createForm('guest_message');
            $formMessage->handleRequest($request);


            if ($id) {
                $guest = $this->getDoctrine()
                ->getManager()
                ->getRepository('MyWeddingGuestBundle:Guest')
                ->findOneById($id);

                 $guests[] = $guest;
            }
            elseif(isset($request->request->all()['id_guest'])){
                $ids = $request->request->all()['id_guest'];
                $guests = $this->getDoctrine()
                ->getManager()
                ->getRepository('MyWeddingGuestBundle:Guest')
                ->findAllGuestsById($ids, $couple->getId());
            }
            if(count($guests)){

                if ($formMessage->isValid()) {
                    foreach ($guests as $key => $guest) {
                    
                        if($guest->getIsDiner()){
                            $links = '<a href="'. $this->get('router')->generate('my_wedding_couple_guest_message_validation', array(
                                'couple'=>$couple->getId(), 
                                'guest' => $guest->getId(), 
                                'type'=>'4'), true).'">Vin d\'honneur et repas</a>';

                            $links .= '<br><a href="'. $this->get('router')->generate('my_wedding_couple_guest_message_validation', array(
                                'couple'=>$couple->getId(), 
                                'guest' => $guest->getId(), 
                                'type'=>'3'), true).'">Vin d\'honneur uniquement</a>';


                            $links .= '<br><a href="'. $this->get('router')->generate('my_wedding_couple_guest_message_validation', array(
                                'couple'=>$couple->getId(), 
                                'guest' => $guest->getId(), 
                                'type'=>'2'), true).'">Ne vient pas</a>';

                        }

                        $mandrillService = $this->container->get('hip_mandrill.dispatcher');

                        $message = new Message();

                        $message
                            ->setFromEmail($this->getUser()->getEmail())
                            ->setFromName('MyWedding')
                            ->addTo($guest->getEmail())
                            ->setSubject($formMessage->get('subject')->getData())
                            ->setHtml('<html><body>'.$formMessage->get('message')->getData().'<br>'.$links.'</body></html>')
                            ->setSubaccount('Register');

                        $mandrillService->send($message);
                    }

                    $this->get('session')->getFlashBag()->add(
                        'success',
                        'Votre message à été enovoyé !'
                    );

                    return $this->redirect($this->generateUrl('my_wedding_couple_guest'));

                }
            }
            else{
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    'Aucun invité selectionné !' 
                );

                return $this->redirect($this->generateUrl('my_wedding_couple_guest'));
            }
            return $this->render('MyWeddingGuestBundle:Guest:message.html.twig', 
                array(
                'guests'=>$guests,
                'formMessage'=>$formMessage->createView()
                )
                );
        
            
        }

        else{
            throw new AccessDeniedHttpException("Accès limité aux futurs marriés");
        }
       
    }


    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

}
