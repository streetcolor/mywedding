<?php

namespace MyWedding\GuestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use MyWedding\GuestBundle\Entity\Guest;
use MyWedding\GuestBundle\Entity\GuestRelation;
use MyWedding\GuestBundle\Form\Type\GuestType;
use MyWedding\GuestBundle\Form\Type\MessageType;
use Hip\MandrillBundle\Message;
use MyWedding\Lib\Tools;


class GuestController extends Controller
{
  
    
    public function indexAction($id=false, Request $request){

        if($this->get('security.context')->isGranted('ROLE_COUPLE')){

            $em = $this->getDoctrine()->getManager();
            $couple = $this->get('mywedding.init.couple')->getCouple();
            
            $guest = new Guest();

            if($this->addMultiAction($request)){

            }

            if ($id) {
                 $guest = $this ->getDoctrine()
                                ->getManager()
                                ->getRepository('MyWeddingGuestBundle:Guest')
                                ->findOneById($id);
            }

            $guest->setCouple($couple);

            $formGuest = $this->createForm(new GuestType($this->getDoctrine(), $couple), $guest);


            if($id)
                $route =  $this->get('router')->generate('my_wedding_couple_guest_edit', array('id' => $id));
            else
                $route = $this->get('router')->generate('my_wedding_couple_guest_add');

            $return = array(
                'formGuest' => $formGuest->createView(),
                'guests' =>$couple->getGuests(),
                'current_id'=>$id,
                'route'=> $route
            );

            return $this->render('MyWeddingGuestBundle:Guest:index.html.twig', $return);  
        
            
        }

        else{
            throw new AccessDeniedHttpException("Accès limité aux futurs marriés");
        }
       
    }



    public function ajaxAction(Request $request){
        $column = [
                        'firstname',
                        'address',
                        'email',
                        'guestRelation',
                        'id_status'
                    ];

        $orderColumn     = $request->request->get('order')[0]["column"];
        $orderTypeColumn = $request->request->get('order')[0]["dir"];
        $couple = $this->get('mywedding.init.couple')->getCouple();

        $draw  = $request->request->get('draw');
        $repository = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('MyWeddingGuestBundle:Guest')->createQueryBuilder('g');

         $guests = $repository
                    ->where('g.couple = :couple')
                    //->outerJoin('MyWeddingGuestBundle:GuestRelation gr', 'c')
                    ->setParameter('couple', $couple)
                    ->andWhere('(g.email LIKE :email OR g.firstname LIKE :firstname OR g.lastname LIKE :lastname)')
                    ->setParameter('email', '%'.$request->request->get('search')["value"].'%')
                    ->setParameter('firstname', '%'.$request->request->get('search')["value"].'%')
                    ->setParameter('lastname', '%'.$request->request->get('search')["value"].'%')
                    ->orderBy('g.'.$column[$orderColumn], $orderTypeColumn)
                    ->setMaxResults($request->request->get('length'))
                    ->setFirstResult($request->request->get('start'));
        

         $query =  $guests
                    
                    ->getQuery()
                    ->getResult();
        
         $count = count($query);



        $table =    [
                        'draw'=>$draw,
                        'recordsTotal'=>$count,
                        'recordsFiltered'=>$count,           
                    ];
        $table['data'] = array();
        foreach ($query as $key => $guest) {
            $relation =  preg_match('#GuestRelation#', get_class($guest->getGuestRelation())) ? $guest->getGuestRelation()->getName() : '';
            $diner =  $guest->getIsDiner() ? 'Oui' : 'Non';
            $notified =  $guest->getIsNotified() ? 'Oui' : 'Non';

            $status = '
                        <div class="guest-status status-'.$guest->getIdStatus().' row"></div>';

            $table['data'][] = [
                                    $guest->getFirstname()." ".$guest->getLastname(),
                                    $guest->getAddress(),
                                    $guest->getEmail(),
                                    $relation,
                                    $status,
                                    $diner,
                                    $notified,
                                    '
                                    <a class="fa fa-pencil" data-target="'. $this->get('router')->generate('my_wedding_couple_guest_edit', array('id' => $guest->getId())).'" href="'. $this->get('router')->generate('my_wedding_couple_guest_edit', array('id' => $guest->getId())).'"></a>
                                    <a class="fa fa-trash" href="'. $this->get('router')->generate('my_wedding_couple_guest_remove', array('id' => $guest->getId())).'"></a>
                                    <a class="fa fa-envelope-o" href="'. $this->get('router')->generate('my_wedding_couple_guest_message', array('id' => $guest->getId())).'"></a>
                                    <input type="checkbox" name="id_guest[]" value="'. $guest->getId().'">
                                    '

                                ];
        }
        $response = new JsonResponse();
        return  $response->setData($table);

    }


    public function editAction($id=null, Request $request){
        if(!$request->isXmlHttpRequest())
            return $this->indexAction($id, $request);
        
        $response = new JsonResponse();
        $guest = new Guest();
        $couple = $this->get('mywedding.init.couple')->getCouple();

        if ($id) {
            $guest = $this->getDoctrine()
            ->getManager()
            ->getRepository('MyWeddingGuestBundle:Guest')
            ->findOneById($id);
        }

        if($request->isXmlHttpRequest() && $request->isMethod('get')){
            $response = new JsonResponse();
            $isDiner = $guest->getIsDiner() ? 1:0;
            $id_relation = $guest->getGuestRelation() instanceof GuestRelation ?  $guest->getGuestRelation()->getId() : false; 
            $return = array(
                    'email'=>$guest->getEmail(),
                    'firstname'=>$guest->getFirstname(),
                    'lastname'=>$guest->getLastname(),
                    'address'=>$guest->getAddress(),
                    'guestRelation'=>$id_relation,
                    'id_status'=>$guest->getIdStatus(),
                    'isDiner'=>$isDiner
                    );
            
            return  $response->setData($return);

        }
        $guest->setCouple($couple);

        $formGuest = $this->createForm(new GuestType($this->getDoctrine(), $couple), $guest);

        $formGuest->handleRequest($request);

        $errors = array();
        $relation = null;
        if(!empty($request->request->get('guestRelation'))){
            $relation = new GuestRelation();
            $relation
                ->setName($request->request->get('guestRelation'))
                ->setCouple($couple);  

            $validator = $this->get('validator');
            $errorList = $validator->validate($relation);

            if (count($errorList) > 0) {
                foreach ($errorList as $error){
                    $errors['guestRelation'] =  [$error->getMessage()];
                }
            } 
           
        }


        if ($formGuest->isValid() && !count($errors)) {
            if(null !==$relation)
                $guest->setGuestRelation($relation);

            $em = $this->getDoctrine()->getManager();

            $em->persist($guest);
            $em->flush();

            return  $response->setData(
                [
                    'status'=>'success',
                    'content'=>$this->getErrorMessages($formGuest)
                ]
                
            );

        }

       $output =  array_merge($errors, $this->getErrorMessages($formGuest));

        return  $response->setData(
            [
                'status'=>'error',
                'content'=>$output
            ]
            
        );

    }

    public function addAction(Request $request){
        if(!$request->isXmlHttpRequest())
            return $this->indexAction($id, $request);
        
        $response = new JsonResponse();
        $guest = new Guest();
        $couple = $this->get('mywedding.init.couple')->getCouple();

        $guest->setCouple($couple);

        $formGuest = $this->createForm(new GuestType($this->getDoctrine(), $couple), $guest);

        $formGuest->handleRequest($request);

        $errors = array();
        $relation = null;
        if(!empty($request->request->get('guestRelation'))){
            $relation = new GuestRelation();
            $relation
                ->setName($request->request->get('guestRelation'))
                ->setCouple($couple);  

            $validator = $this->get('validator');
            $errorList = $validator->validate($relation);

            if (count($errorList) > 0) {
                foreach ($errorList as $error){
                    $errors['guestRelation'] =  [$error->getMessage()];
                }
            } 
           
        }


        if ($formGuest->isValid() && !count($errors)) {
            if(null !==$relation)
                $guest->setGuestRelation($relation);

            $em = $this->getDoctrine()->getManager();

            $em->persist($guest);
            $em->flush();

            return  $response->setData(
                [
                    'status'=>'success',
                    'content'=>$this->getErrorMessages($formGuest)
                ]
                
            );

        }

       $output =  array_merge($errors, $this->getErrorMessages($formGuest));

        return  $response->setData(
            [
                'status'=>'error',
                'content'=>$output
            ]
            
        );
    }


    public function removeAction($id, Request $request){

        $em = $this->getDoctrine()->getManager();

        if (null!==$id) {

            $guest = $this->getDoctrine()
            ->getManager()
            ->getRepository('MyWeddingGuestBundle:Guest')
            ->findOneById($id);

            $em->remove($guest);
            $em->flush();
            $em->clear();

        }

        $request->getSession()->getFlashBag()->add('info', 'Invité bien supprimé.');

        return $this->redirect($this->generateUrl('my_wedding_couple_guest'));

    }

    public function addMultiAction(Request $request){

        $couple = $this->get('mywedding.init.couple')->getCouple();
         $em = $this->getDoctrine()->getManager();

        if(!empty($request->request->get('emails'))){

            $emails = explode(PHP_EOL, $request->request->get('emails'));
            foreach ($emails as $key => $email) {
                $guest = new Guest();
                $guest
                    ->setCouple($couple)
                    ->setEmail(trim($email));    

                $validator = $this->get('validator');
                $errorList = $validator->validate($guest);

                if (!count($errorList)) {
                    $em->persist($guest);
                } 
            
            }

             $em->flush();

             return true;

        }

        return false;
    }

    private function getErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

}
