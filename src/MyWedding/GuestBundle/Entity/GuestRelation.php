<?php

namespace MyWedding\GuestBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
* Couple
*
* @ORM\Table(name="GuestRelation",uniqueConstraints={@ORM\UniqueConstraint(name="unique", columns={"name", "couple_id"})})
* @ORM\Entity(repositoryClass="MyWedding\GuestBundle\Entity\GuestRelationRepository")
* @UniqueEntity(fields={"name","couple"}, message="Ce groupe est déjà enregistré")
*/
class GuestRelation{
    
    
    /**
    * @ORM\ManyToOne(targetEntity="MyWedding\UserBundle\Entity\Couple", cascade={"persist"})
     * @ORM\JoinColumn(name="couple_id", referencedColumnName="id")
    */
    private $couple;

     /**
     * @ORM\OneToMany(targetEntity="Guest", mappedBy="guestRelation")
     **/
    private $guest;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(groups={"save"})
     * @Assert\Email(groups={"update"})
     */
    protected $name;
    


    public function __toString(){
        
        return $this->getName();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        if(null==$this->id)
            return 0;
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GuestRelation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guest
     *
     * @param \MyWedding\GuestBundle\Entity\Guest $guest
     * @return GuestRelation
     */
    public function setGuest(\MyWedding\GuestBundle\Entity\Guest $guest = null)
    {
        $this->guest = $guest;

        return $this;
    }

    /**
     * Get guest
     *
     * @return \MyWedding\GuestBundle\Entity\Guest 
     */
    public function getGuest()
    {
        return $this->guest;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->guest = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set couple
     *
     * @param \MyWedding\UserBundle\Entity\Couple $couple
     *
     * @return GuestRelation
     */
    public function setCouple(\MyWedding\UserBundle\Entity\Couple $couple = null)
    {
        $this->couple = $couple;
        return $this;
    }

    /**
     * Get couple
     *
     * @return \MyWedding\UserBundle\Entity\Couple
     */
    public function getCouple()
    {
        return $this->couple;
    }

    /**
     * Add guest
     *
     * @param \MyWedding\GuestBundle\Entity\Guest $guest
     *
     * @return GuestRelation
     */
    public function addGuest(\MyWedding\GuestBundle\Entity\Guest $guest)
    {
        $this->guest[] = $guest;

        return $this;
    }

    /**
     * Remove guest
     *
     * @param \MyWedding\GuestBundle\Entity\Guest $guest
     */
    public function removeGuest(\MyWedding\GuestBundle\Entity\Guest $guest)
    {
        $this->guest->removeElement($guest);
    }
}
