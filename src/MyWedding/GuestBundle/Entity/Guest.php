<?php

namespace MyWedding\GuestBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Couple
 *
 * @ORM\Table(name="Guest",uniqueConstraints={@ORM\UniqueConstraint(name="unique", columns={"email", "couple_id"})})
 * @ORM\Entity(repositoryClass="MyWedding\GuestBundle\Entity\GuestRepository")
 * @UniqueEntity(fields={"email","couple"}, message="Cette address est déjà enregistré")
 */
class Guest{
    
    
    /**
    * @ORM\ManyToOne(targetEntity="GuestRelation", inversedBy="guest", cascade={"persist"})
    * @ORM\JoinColumn(name="guest_relation_id", referencedColumnName="id")
    **/
    private $guestRelation;
    
    /**
    * @ORM\ManyToOne(targetEntity="MyWedding\UserBundle\Entity\Couple", inversedBy="guests")
    * @ORM\JoinColumn(name="couple_id", referencedColumnName="id")
    */
    protected $couple;
    
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    
    /**
    * @ORM\Column(type="integer", options={"default" = 1})
    * @ORM\GeneratedValue(strategy="AUTO")
    * @Assert\NotBlank()
    */
    protected $id_status;

    /**
    * @ORM\Column(type="boolean", options={"default" = 1})
    * @ORM\GeneratedValue(strategy="AUTO")
    * @Assert\NotBlank()
    */
    protected $isDiner;


    /**
    * @ORM\Column(type="boolean", options={"default" = 0})
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $isNotified;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $email;

    /**
    * @ORM\Column(type="string", nullable=true,  length=255)
    * @Assert\Length(min=2)
    */
    protected $firstname = null;

    /**
    * @ORM\Column(type="string", nullable=true,  length=255)
    * @Assert\Length(min=2)
    */
    protected $lastname = null;
    
    /**
    * @ORM\Column(type="string", nullable=true,  length=255)
    * @Assert\Length(min=2)
    */
    protected $address = null;
    

    public function __construct($type=null){
        $this->isNotified = 0;
        $this->id_status = 1;
        $this->isDiner = 1;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Guest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Guest
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Guest
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Guest
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set couple
     *
     * @param \MyWedding\UserBundle\Entity\Couple $couple
     * @return Guest
     */
    public function setCouple(\MyWedding\UserBundle\Entity\Couple $couple = null)
    {
        $this->couple = $couple;

        return $this;
    }

    /**
     * Get couple
     *
     * @return \MyWedding\UserBundle\Entity\Couple 
     */
    public function getCouple()
    {
        return $this->couple;
    }

   

    /**
     * Set guestRelation
     *
     * @param \MyWedding\GuestBundle\Entity\GuestRelation $guestRelation
     * @return Guest
     */
    public function setGuestRelation(\MyWedding\GuestBundle\Entity\GuestRelation $guestRelation = null)
    {
        $this->guestRelation = $guestRelation;

        return $this;
    }

    /**
     * Get guestRelation
     *
     * @return \MyWedding\GuestBundle\Entity\GuestRelation 
     */
    public function getGuestRelation()
    {
        return $this->guestRelation;
    }

    /**
     * Set idStatus
     *
     * @param integer $idStatus
     *
     * @return Guest
     */
    public function setIdStatus($idStatus)
    {   
        $this->id_status = !$idStatus  ? 1 : $idStatus;

        return $this;
    }

    /**
     * Get idStatus
     *
     * @return integer
     */
    public function getIdStatus()
    {
        return $this->id_status;
    }

    /**
     * Set isDiner
     *
     * @param boolean $isDiner
     *
     * @return Guest
     */
    public function setIsDiner($isDiner)
    {
        $this->isDiner = $isDiner === null ? 1 : $isDiner;;

        return $this;
    }

    /**
     * Get isDiner
     *
     * @return boolean
     */
    public function getIsDiner()
    {
        return $this->isDiner;
    }

    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     *
     * @return Guest
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;

        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }
}
