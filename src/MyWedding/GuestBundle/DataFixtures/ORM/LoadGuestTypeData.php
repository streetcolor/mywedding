<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Acme/HelloBundle/DataFixtures/ORM/LoadUserData.php

namespace MyWedding\CoupleBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MyWedding\CoupleBundle\Entity\GuestType;

class LoadGuestTypeData implements FixtureInterface
{
    /**
    * {@inheritDoc}
    */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new GuestType();
        $userAdmin->setName('Grand-Mere');
        $manager->persist($userAdmin);
        
        $userAdmin->setName('Grand-Pere');
        $manager->persist($userAdmin);
        
        $userAdmin->setName('Cousin');
        $manager->persist($userAdmin);
        
        $userAdmin->setName('Cousine');
        $manager->persist($userAdmin);
        
        $manager->flush();
    }
}
