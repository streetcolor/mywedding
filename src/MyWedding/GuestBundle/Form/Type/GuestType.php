<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/MyWedding/GuestBundle/Form/Type/GuestType.php
namespace MyWedding\GuestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceListInterface;
use MyWedding\GuestBundle\Entity\GuestRelation;

class GuestType extends AbstractType
{
    
    
    /** @var \Doctrine\ORM\EntityManager */
    private $couple;
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /**
     * Constructor
     * 
     * @param Doctrine $doctrine
     */
    public function __construct(Doctrine $doctrine, $couple=false)
    {   
        $this->couple =  $couple;
        $this->em = $doctrine->getManager();
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\GuestBundle\Entity\Guest',
            /*'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();
                //EX : $data->getEmail()=='sebastien.tetard@yahoo.fr'
                //EX : $form->get('type')->getData()
                if($form->get('type')->getData()==1){
                    return array('update');
                }
                else{
                    return array('save');
                }
            },*/
        ));
        
       
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'text');
        $builder->add('firstname', 'text');
        $builder->add('lastname', 'text');
        $builder->add('address', 'textarea');
        $builder->add('id_status', 'choice', array(
            'choices' => $this->getListStatus(),
            'empty_value' => 'Presence',
            "attr"=>array('class'=>'selectpicker show-tick col-sm-10 form-control')

        ));
        $builder->add('isDiner', 'choice', array(
            'choices' => array('Oui', 'Non'),
             'empty_value' => 'Invité au diné',
            "attr"=>array('class'=>'selectpicker show-tick form-control')

        ));
        $builder->add('guestRelation', 'entity', array(
            'choices'    =>  $this->getListRelation(),
            'mapped'   => true, 
            'class'    => 'MyWeddingGuestBundle:GuestRelation',
            'empty_value' => 'Choisissez votre relation',
            'required'    => false,
            "attr"=>array('class'=>'selectpicker show-tick form-control')
          ));
      
    }

    public function getListRelation(){
        $return = array();
        $relations = $this->em->getRepository('MyWeddingGuestBundle:GuestRelation')->findByCouple($this->couple);
        $other = new GuestRelation();
       
        foreach ($relations as $relation){
            $return[$relation->getId()] = $relation;
        }
           return $return; 
        
    }

    public function getListStatus(){
        
        $status = array(
                            'Non repondu', 
                            'PAs décide',
                            'Ne viens pas',
                            'Vin d\'honneur uniquement',
                            'Vin d\'honneur et repas'
                        );

           return $status; 
        
    }

    public function getName()
    {
        return 'guest_creation';
    }
   
     
    
}
