<?PHP
// src/Acme/TaskBundle/Form/Type/TaskType.php
namespace MyWedding\GuestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('subject', 'text', array(
		        'constraints' => array(
		            new NotBlank(),
		        )
		    ))
	      	->add('message', 'textarea', array(
		        'constraints' => array(
		            new NotBlank(),
		        )
		    ));
    }

    public function getName()
    {
        return 'guest_message';
    }
}