<?php

namespace MyWedding\BudgetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !


class BudgetController extends Controller
{
    public function indexAction()
    {
        return $this->render('MyWeddingBudgetBundle:Budget:index.html.twig');
    }


    public function ajaxModalAddAction(Request $request){
        return $this->render('MyWeddingBudgetBundle:Modals:add.html.twig');
    }
}
