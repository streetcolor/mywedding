<?PHP
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MyWedding\FrontBundle\Entity\Couple;

class MyFixtures implements FixtureInterface, ContainerAwareInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {

        $userManager = $this->container->get('fos_user.user_manager');
        
        
        $married = new Couple();
        $married->setBudget(500000);
        $married->setGuests(200);
        $married->setDateCreated(new DateTime());
        $married->setDateUpdated(new Datetime());
        $user = $userManager->createUser();
        $user->setUsername('sebastien');
        $user->setEmail('sebastien@myweddingplanning.fr');
        $user->setPlainPassword('sebastien');
        $user->setRoles(array("ROLE_MARRIED"));
        $user->setEnabled(true);
        
        $married->setUser($user);
        
        $manager->persist($married);
        
        
                
         // Create a new user
        $married = new Couple();
        $married->setBudget(500000);
        $married->setGuests(200);
        $married->setDateCreated(new DateTime());
        $married->setDateUpdated(new Datetime());
        $user = $userManager->createUser();
        $user->setUsername('jean');
        $user->setEmail('jean@myweddingplanning.fr');
        $user->setPlainPassword('jean');
        $user->setRoles(array("ROLE_MARRIED"));
        $user->setEnabled(true);
                $married->setUser($user);

        $manager->persist($married);

        
         // Create a new user
        $married = new Couple();
        $married->setBudget(500000);
        $married->setGuests(200);
        $married->setDateCreated(new DateTime());
        $married->setDateUpdated(new Datetime());
        $user = $userManager->createUser();
        $user->setUsername('aurore');
        $user->setEmail('aurore@myweddingplanning.fr');
        $user->setPlainPassword('aurore');
        $user->setRoles(array("ROLE_PROVIDER"));
        $user->setEnabled(true);
        $married->setUser($user);

        $manager->persist($married);

         
         
        $manager->flush();
    }
}