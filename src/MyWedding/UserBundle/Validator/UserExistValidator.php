<?php

namespace MyWedding\UserBundle\Validator;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @author Sebastien Tetard <sebastien.tetard@yahoo.fr>
 *
 * @api
 */
class UserExistValidator extends ConstraintValidator
{
    
    private $requestStack;
    private $em;
    private $securityContext;
    // Les arguments déclarés dans la définition du service arrivent au constructeur
    // On doit les enregistrer dans l'objet pour pouvoir s'en resservir dans la méthode validate()

    public function __construct(RequestStack $requestStack, EntityManagerInterface $em,SecurityContextInterface $securityContext){
       
      $this->requestStack = $requestStack;
      $this->em           = $em;
      $this->securityContext = $securityContext;
    }
    
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
                
        if (!$constraint instanceof UserExist) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\UserExist');
        }

        if ('' !== $value && null !== $value) {
            
            $id      =  $this->securityContext->getToken()->getUser()->getId();
            $email   = $value;
            
            $em = $this->em;
            $query = $em->createQuery(
                'SELECT u
                FROM MyWeddingUserBundle:User u
                WHERE u.email = :email AND u.id != :id'
            )
            ->setParameter('email', $email)
            ->setParameter('id', $id);


            $products = $query->getResult();
            
            if(count($products)){
                $this->buildViolation($constraint->message)
                    ->setParameters(array('%string%' => $value))
                    ->addViolation();
            }
        }
    }
}
