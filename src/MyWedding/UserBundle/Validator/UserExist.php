<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MyWedding\UserBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Sebastien Tetard <sebastien.tetard@yahoo.fr>
 *
 * @api
 */
class UserExist extends Constraint
{
    public $message = 'Cet utilisateur existe déjà';
    
    public function validatedBy()
    {
      return 'mywedding_couple_userexist'; // Ici, on fait appel à l'alias du service
    }
}
