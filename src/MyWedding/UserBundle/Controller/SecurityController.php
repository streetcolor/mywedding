<?php
// src/Sdz/UserBundle/Controller/SecurityController.php;

namespace MyWedding\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use MyWedding\UserBundle\Entity\Couple;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

use Hip\MandrillBundle\Message;
use Hip\MandrillBundle\Dispatcher;
use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{
 
    public function loginAction(Request $request)
    {
  
        $registration = $this->container->get('mywedding.registration');
        $form         = $registration->MakeRegistration();
       
        if($form instanceof RedirectResponse){
            return $form;
        }   
        
        $connexion      = $this->container->get('mywedding.connexion')->MakeConnexion();
        

        $csrfToken = $this->has('form.csrf_provider')
            ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;

        return $this->renderLogin(array(
            'type'=>$request->get('type'),
            'last_username' => $connexion["username"],
            'error'         => $connexion["error"],
            'csrf_token' => $csrfToken,
        ));
    }
    
    
    /**
   * On modifie la façon dont est choisie la vue lors du rendu du formulaire de connexion
   *
   * Je sais que c'est cette méthode qu'il faut hériter car j'ai été voir le contrôleur d'origine du bundle :
   * https://github.com/FriendsOfSymfony/FOSUserBundle/blob/master/Controller/SecurityController.php
   */
  protected function renderLogin(array $data)
  {
      
    
    // Sur la page du formulaire de connexion, on utilise la vue classique "login"
    // Cette vue hérite du layout et ne peut donc être utilisée qu'individuellement
    if ($this->container->get('request')->attributes->get('_route') == 'my_wedding_front_login') {
      $view = 'login';
    
    } else {
      // Mais sinon, il s'agit du formulaire de connexion intégré au menu, on utilise la vue "login_content"
      // car il ne faut pas hériter du layout !
      $view = 'login_content';
    }

    $template = ('FOSUserBundle:Security:'.$view.'.html.twig');
    return $this->container->get('templating')->renderResponse($template, $data);
  }
  

  
    public function registerAction(Request $request){

        $registration = $this->container->get('mywedding.registration');
        $form         = $registration->MakeRegistration();

        return $this->container->get('templating')->renderResponse('MyWeddingUserBundle:Security:register_content.html.twig', 
                array(
                    'type'=>$request->get('type'),
                    'form_register'=>$form->createView()
                ));
    }
}
