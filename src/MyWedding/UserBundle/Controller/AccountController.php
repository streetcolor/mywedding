<?php
namespace MyWedding\UserBundle\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !


class AccountController extends Controller
{

    public function indexAction(Request $request){

        if($this->get('security.context')->isGranted('ROLE_COUPLE')){
            return $this->render('MyWeddingUserBundle:Account:account.html.twig');
        }
        else{
            throw new AccessDeniedHttpException("Accès limité aux futurs marriés");
        }

       
    }

    public function accountAction(){
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
       
        $formUser = $this->createForm('mywedding_user_edition', $user);
        $formUser->setData($user);

        $formUser->handleRequest($request);

        if ($formUser->isValid()) {
            $em->persist($user);
            $em->flush();
        }
       
        return $this->render('MyWeddingUserBundle:Account:edit_account.html.twig', array(
            'formCouple' => $formUser->createView()
        ));

       
    }  

    public function passwordAction(){
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $formFactory = $this->get('fos_user.change_password.form.factory');
        $formPassword = $formFactory->createForm();
        $formPassword->setData($user);
        $formPassword->handleRequest($request);

        if ($formPassword->isValid()) {
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
        }
        

        return $this->render('MyWeddingUserBundle:Account:edit_password.html.twig', array(
           'formPassword' => $formPassword->createView()
        ));

    }

   
}
