<?php

namespace MyWedding\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MyWeddingUserBundle extends Bundle
{

  public function getParent()
  {
    return 'FOSUserBundle';
  }
  
}
