<?php
namespace MyWedding\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
//use MyWedding\CoupleBundle\Entit\AbstractGoogleMap;

/**
 * Couple
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MyWedding\UserBundle\Entity\CoupleRepository")
 */
class Couple 
{   

    /**
    * @var string
    *
    * @ORM\Column(name="address", type="string", length=255, nullable=true)
    */
    protected $address;
       
    /**
     * @ORM\OneToMany(targetEntity="MyWedding\GuestBundle\Entity\Guest", mappedBy="couple")
     */
    protected $guests;
    
    /**
    * @ORM\OneToOne(targetEntity="MyWedding\UserBundle\Entity\User", cascade={"persist"})
    */
    private $user;

    /**
    * @ORM\OneToOne(targetEntity="MyWedding\ProfileBundle\Entity\Document", cascade={"persist"})
    * @ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="SET NULL") 
    * @Assert\File
    */
    private $document;
    
    /**
    * @var integer
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
    * @var string
    *
    * @ORM\Column(name="wife", type="string")
    */
    private $wife;

    /**
    * @var string
    *
    * @ORM\Column(name="husband", type="string")
    */
    private $husband;

    /**
    * @var date
    *
    * @ORM\Column(name="date_event", type="date")
    */
    private $dateEvent;

    /**
    * @var integer
    *
    * @ORM\Column(name="guestNumber", type="integer")
    * @Assert\Type(type="digit",  groups={"save"})
    * @Assert\Type(type="digit", groups={"update"})
    */
    private $guestNumber;

    /**
    * @var integer
    *
    * @ORM\Column(name="budget", type="integer")
    * @Assert\Type(type="digit",  groups={"save"})
    * @Assert\Type(type="digit", groups={"update"})
    */
    private $budget;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="date_created", type="datetime")
    */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_updated", type="datetime")
     */
    private $dateUpdated;
    
    public function __construct()
    {
    
        $this->guestNumber = 0;
        $this->budget = 0;
        $this->wife = 0;
        $this->husband = 0;
        $this->dateEvent = new \DateTime('2000-01-01');;
        $this->dateCreated = new \DateTime("now");
        $this->dateUpdated = new \DateTime("now");
        $this->guests      = new ArrayCollection();

        
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guestNumber
     *
     * @param integer $guestNumber
     * @return Couple
     */
    public function setGuestNumber($guestNumber)
    {
        $this->guestNumber = $guestNumber;

        return $this;
    }

    /**
     * Get guestNumber
     *
     * @return integer 
     */
    public function getGuestNumber()
    {
        return $this->guestNumber;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     * @return Couple
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return integer 
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Couple
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     * @return Couple
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime 
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set user
     *
     * @param \MyWedding\UserBundle\Entity\User $user
     * @return Couple
     */
    public function setUser(\MyWedding\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MyWedding\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add guests
     *
     * @param \MyWedding\GuestBundle\Entity\Guest $guests
     * @return Couple
     */
    public function addGuest(\MyWedding\GuestBundle\Entity\Guest $guests)
    {
        $this->guests[] = $guests;

        return $this;
    }

    /**
     * Remove guests
     *
     * @param \MyWedding\GuestBundle\Entity\Guest $guests
     */
    public function removeGuest(\MyWedding\GuestBundle\Entity\Guest $guests)
    {
        $this->guests->removeElement($guests);
    }

    /**
     * Get guests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * Set wife
     *
     * @param string $wife
     * @return Couple
     */
    public function setWife($wife)
    {
        $this->wife = $wife;

        return $this;
    }

    /**
     * Get wife
     *
     * @return string 
     */
    public function getWife()
    {
        return $this->wife;
    }

    /**
     * Set husband
     *
     * @param string $husband
     * @return Couple
     */
    public function setHusband($husband)
    {
        $this->husband = $husband;

        return $this;
    }

    /**
     * Get husband
     *
     * @return string 
     */
    public function getHusband()
    {
        return $this->husband;
    }

   

    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     * @return Couple
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime 
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }
    

    /**
     * Set address
     *
     * @param string $address
     * @return Couple
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }



    /**
     * Set document
     *
     * @param \MyWedding\ProfileBundle\Entity\Document $document
     * @return Couple
     */
    public function setDocument(\MyWedding\ProfileBundle\Entity\Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \MyWedding\ProfileBundle\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }
}
