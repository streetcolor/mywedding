<?php
// src/OC/PlatformBundle/Antispam/OCAntispam.php

namespace MyWedding\UserBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Hip\MandrillBundle\Message;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use MyWedding\UserBundle\Entity\Couple;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Event\FilterUserResponseEvent;

use FOS\UserBundle\FOSUserEvents;


class Registration
{

    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    private $container;
    private $request;
    
    public function __construct(Container $container, Doctrine $doctrine) {
        $this->container = $container;
        $this->em        = $doctrine->getManager();
        $this->request   = $this->container->get('request');
         
    }
    
    public function SendEmail($user, $password){
         $mandrillService = $this->container->get('hip_mandrill.dispatcher');

        $message = new Message();

        $message
            ->setFromEmail('contact@myweddingplannning.fr')
            ->setFromName('Customer ddddCare')
            ->addTo($user->getEmail())
            ->setSubject('Inscription')
            ->setHtml('<html><body><h1>Somedddd Content</h1>Login : '.$user->getEmail().' Password : '.$password.'</body></html>')
            ->setSubaccount('Register');

        $mandrillService->send($message);
    }
    
    
    public function MakeRegistration(){
        
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $form = $this->container->get('fos_user.registration.form.factory')->createForm();
         
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

       
        $user = $userManager->createUser();
        $user->setEnabled(true);
      
        $password = "test";
        
        $form->setData($user);
        $user->setPassword($password);
        $user->setPlainPassword($password);
        $user->setRoles(array("ROLE_COUPLE"));
        
   
        if ($form->handleRequest($this->request)->isValid()) {
         
            $userManager->updateUser($user);
            
            // On récupère l'EntityManager
            $em = $this->em;
            $couple = new Couple();
            $couple->setUser($user);
            $em->persist($couple);
            $em->flush();
            
            $this->SendEmail($user, $password);
           
            $url =  $this->container->get('router')->generate('fos_user_registration_confirmed');
            
            $response = new RedirectResponse($url);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $this->request, $response));
            
            return $response;
        }
    
        
        return $form;
    }
    
}