<?php
// src/OC/PlatformBundle/Antispam/OCAntispam.php

namespace MyWedding\UserBundle\Services;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\SecurityContextInterface;

class Connexion
{

    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    private $container;
    private $request;
    private $session;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->request   = $this->container->get('request');
        $this->session  =  $this->request->getSession();
    }
    

    public function MakeConnexion(){
                
        // get the error if any (works with forward and redirect -- see below)
        if ($this->request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $this->request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        } elseif (null !== $this->session && $this->session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $this->session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $this->session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = null;
        }

        
        $lastUsername = (null === $this->session) ? '' : $this->session->get(SecurityContextInterface::LAST_USERNAME);
        
        return array('error'=>$error, 'username'=>$lastUsername);
    }
    
}