<?PHP

namespace MyWedding\UserBundle\Services;
 
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use MyWedding\UserBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContext;

 
class CoupleInit
{
    protected $context;
    protected $request;
    protected $couple;
    protected $em;

    public function __construct(SecurityContext $context, RequestStack $request,  Doctrine $doctrine)
    {

        $this->context =       $context;
        $this->request =       $request;
        $this->em      =       $doctrine->getManager();

      
    }
 
    public function setCouple(FilterControllerEvent $event)
    {  

        // 

        //ONLY load on the main request 
        if(HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()){
            return;
        }
        //set couple One Time
        if ($this->context->getToken()) {
            if($this->context->isGranted('ROLE_COUPLE')){

                $user = $this->context->getToken()->getUser();

                $this->couple = $this->em
                  ->getRepository('MyWeddingUserBundle:Couple')
                  ->findOneByUser($user);

            }
           
        }
           
    
      
    }

    public function getCouple()
    {

                return $this->couple;
        
    }
}