<?php

namespace MyWedding\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CoupleFormType extends AbstractType
{
        
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\UserBundle\Entity\Couple'
        ));
          
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('guestNumber', 'text');
        $builder->add('budget', 'text');
        $builder->add('user', new ProfileFormType());


    }

    public function getName()
    {
        return 'mywedding_couple_edition';
    }
}
