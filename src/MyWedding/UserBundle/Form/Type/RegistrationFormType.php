<?php
 
namespace MyWedding\UserBundle\Form\Type;
 
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\HttpFoundation\RequestStack;

class RegistrationFormType extends BaseType
{
    private $request;
        
    public function __construct($class, RequestStack $request)
    {
        $this->request = $request;
        parent::__construct($class);
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->request->getMasterRequest()->get('_route')=='my_wedding_front_login'){
            $class  = 'col-xs-12';
        } 
        else{
            $class  = 'form-control';

        }
        parent::buildForm($builder, $options);
 
        // add your custom field
        $builder->add('email', 'email',  array('attr' => array('class' => $class)));
        $builder->remove('password');
        $builder->remove('username');
        $builder->remove('plainPassword');
  
    }
 
    public function getName()
    {
        return 'heavym_user_registration';
    }
}