<?php


namespace MyWedding\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use MyWedding\UserBundle\Validator\UserExist;


class ProfileFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildUserForm($builder, $options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\UserBundle\Entity\User',
            'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();
               // if(null==$data->getFacebookId())
                    return array('Default');

            },
        ));
        
       
    }

    public function getName()
    {
        return 'mywedding_user_edition';
    }

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
      
        $builder->add('username', null, array(
                                    'label' => 'form.username', 
                                    'translation_domain' => 'FOSUserBundle', 
                                    'attr'=>array(
                                            'class'=>'form-control'
                                            )
                                    ));
        
        
        
        $builder->addEventListener(
           FormEvents::PRE_SET_DATA,
           function ( $event) {
               $form = $event->getForm();
               $data = $event->getData();

               $social = $data->getFacebookId() || $data->getGoogleId();
               
                    $form->add(
                        'email', 'email', array(
                            'label' => 'form.email', 
                            'translation_domain' => 'FOSUserBundle',
                            'attr'=>array(
                                            'class'=>'form-control'
                                        ),
                            'constraints' =>  array(
                                new NotBlank(array('groups' => 'Default')),
                                new UserExist(array('groups' => 'Default'))
                            ),

                        )
                    );
                    
                
              
            }
        );


        
    }
}
