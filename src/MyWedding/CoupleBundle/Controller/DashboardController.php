<?php
namespace MyWedding\CoupleBundle\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !


class DashboardController extends Controller
{


    public function indexAction(Request $request){

        if($this->get('security.context')->isGranted('ROLE_COUPLE')){

            return $this->render('MyWeddingCoupleBundle:Dashboard:index.html.twig');
        }
        else{
            throw new AccessDeniedHttpException("Accès limité aux futurs marriés");
        }


       
    }
   
}
