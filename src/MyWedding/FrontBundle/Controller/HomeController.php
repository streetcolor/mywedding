<?php


namespace MyWedding\FrontBundle\Controller;

use MyWedding\UserBundle\Entity\Couple;
use MyWedding\UserBundle\Entity\User;


use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !
use Symfony\Component\HttpFoundation\RedirectResponse; // N'oubliez pas ce use
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Validator\Constraints\DateTime;

use Hip\MandrillBundle\Message;
use Hip\MandrillBundle\Dispatcher;

class HomeController extends Controller{
    
    // La route fait appel à OCPlatformBundle:Advert:view,
    // on doit donc définir la méthode viewAction.
    // On donne à cette méthode l'argument $id, pour
    // correspondre au paramètre {id} de la route
  
    
    public function indexAction(Request $request){
        $couple= array();

        if($this->get('security.context')->isGranted('ROLE_COUPLE')){
            return $this->redirect($this->generateUrl('my_wedding_couple_dashboard'));
        }
       
        return $this->render('MyWeddingFrontBundle:Home:index.html.twig', 
                    array(
                        "couple"=>'rrr'
                      
                    )
            );
    }
    
    public function infoAction(Request $request){
       
        return $this->render('MyWeddingFrontBundle:Home:info.html.twig', array('form'=>""));
         
    }

   
} 