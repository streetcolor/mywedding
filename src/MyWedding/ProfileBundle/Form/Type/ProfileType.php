<?php

namespace MyWedding\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Ivory\GoogleMap\Places\AutocompleteComponentRestriction;
use Ivory\GoogleMap\Places\AutocompleteType;
class ProfileType extends AbstractType
{
        
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\UserBundle\Entity\Couple',
            'validation_groups' => function(FormInterface $form) {
                //$data = $form->getData();
                //EX : $data->getEmail()=='sebastien.tetard@yahoo.fr'
                //EX : $form->get('type')->getData()
                /*if($form->get('save')->isClicked()){
                    return array('update');
                }
                else{
                    return array('upload');
                }*/
            
            },
        ));
        
       
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('wife', 'text');
        $builder->add('husband', 'text');
        $builder->add('dateEvent', 'date', array(
                                                'widget' => 'single_text',
                                                'input' => 'datetime',
                                                'format' => 'dd/MM/yyyy',
                                                'attr' => array('class' => 'date form-control'),
                                                ));
        $builder->add('guestNumber', 'text');
        $builder->add('budget', 'text');
        //$builder->add('user', new ProfileFormType());

        $builder->add('address', 'places_autocomplete', array(

            // Javascript prefix variable
            'prefix' => 'js_prefix_',

            // Autocomplete bound (array|Ivory\GoogleMap\Base\Bound)
           // 'bound'  => $bound,

            // Autocomplete types
            'types'  => array(
                AutocompleteType::CITIES,
                // ...
            ),

            // Autocomplete component restrictions
            'component_restrictions' => array(
                AutocompleteComponentRestriction::COUNTRY => 'fr',
                // ...
            ),

            // TRUE if the autocomplete is loaded asynchonously else FALSE
            'async' => true,

            // Autocomplete language
            'language' => 'fr',

            "attr"=>array('class'=>'form-control')
        ));

      //  $builder->add('document', new DocumentType());
/*
        $builder->add('save', 'submit', array(
            'attr' => array('class' => 'save', 'value'=>'save'),
        ));

*/
    }

    public function getName()
    {
        return 'mywedding_profile_edition';
    }
}
