<?php
// src/OC/PlatformBundle/Form/ImageType.php

namespace MyWedding\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('file', 'file', array(
                                'label'=>false,
                                'attr' => array('class' => 'ajax_file hidden', 'data-target'=>$options['route']),
        ))
      ->add('upload', 'submit', array(
            'attr' => array('class' => 'upload none', 'value'=>'upload'),
        ));

  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {

    //http://symfony.com/doc/2.2/components/options_resolver.html
     $resolver->setOptional(array(
        'route',
    ));

    $resolver->setDefaults(array(
      'data_class' => 'MyWedding\ProfileBundle\Entity\Document',
      'route'=>null
    ));

  }

  public function getName()
  {
        return 'mywedding_couple_avatar';
  }
}