<?php
namespace MyWedding\ProfileBundle\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request; // N'oubliez pas ce use !
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProfileBundle\Entity\Document;
use MyWedding\ProfileBundle\Form\Type\DocumentType;
use MyWedding\UserBundle\Entity\Couple;

class ProfileController extends Controller
{

    public function indexAction(Request $request){

        if($this->get('security.context')->isGranted('ROLE_COUPLE')){
            if($request->get("_route") == 'my_wedding_couple_profile'){
                return $this->render('MyWeddingProfileBundle:Profile:profile.html.twig');
            }
        }
        else{
            throw new AccessDeniedHttpException("Accès limité aux futurs marriés");
        }

       
    }

    public function uploadAction(Request $request){
        $user = $this->getUser();

        $couple = $this->get('mywedding.init.couple')->getCouple();
        $em = $this->getDoctrine()->getManager();
        $avatar = new Document('avatar');

        if(!$request->isXmlHttpRequest()) {
            $pseudo = $couple->getHusband() && $couple->getWife() ?  $couple->getWife()." & ".$couple->getHusband() : $user->getUsername();

            $formAvatar = $this->createForm('mywedding_couple_avatar', $avatar, array('route'=>$this->get('router')->generate('my_wedding_couple_upload')));
            $formAvatar->handleRequest($request);

            if($couple->getDocument() !== null){
                $avatar = $couple->getDocument();
            }
            else{
                $avatar = false; 
            }

            return $this->render('MyWeddingProfileBundle:Profile:edit_avatar.html.twig', 
            array(
                'current_date'=>date("D d M Y"), 'pseudo'=>$pseudo,      
                'formAvatar' => $formAvatar->createView(),
                'avatar'     => $avatar
                )
            );


        }
            
        $image = $request->files->get('file');

        if (($image->getError() == '0')) {

            if (($image->getSize() < 2000000000)) {
                $originalName = $image->getClientOriginalName();

                $valid_filetypes = array('image/jpeg', 'image/png');
                if (in_array(strtolower($image->getMimeType()), $valid_filetypes)) {
                    //Start Uploading File
                    if($couple->getDocument()){
                        $removeAvatar = $couple->getDocument();
                        $em->remove($removeAvatar);
                    } 
                  $avatar->setFile($image);
                 
                   $couple->setDocument($avatar);

                    $em->persist($couple);
                    $em->flush();
                    $status = 'success';
                    $message = '/web/'.$avatar->getWebPath() ;

                } else {
                    $status = 'error';
                    $message = 'Invalid File Type';
                }
            } 
            else {
                $status = 'error';
                $message = 'Size exceeds limit';
            }
        } 
        else {
            $status = 'error';
            $message = 'File Error';
        }

        // Si le formulaire n'est pas valide on retourne une erreur en JSON
        return new JsonResponse(array(
            'status' =>  $status,
            'content' =>  $message,
        ));
    }

    public function removeAvatarAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $couple = $this->get('mywedding.init.couple')->getCouple();
        $avatar = $couple->getDocument();

        $em->remove($avatar);

        $em->flush();
        $em->clear();
        

        $request->getSession()->getFlashBag()->add('info', 'Avatar bien supprimé.');

        return $this->redirect($this->generateUrl('my_wedding_couple_profile'));

    }  

    public function profileAction(){
        $request = Request::createFromGlobals();

        $couple = $this->get('mywedding.init.couple')->getCouple();

        $em = $this->getDoctrine()->getManager();

        $formCouple = $this->createForm('mywedding_profile_edition', $couple);
        $formCouple->setData($couple);

        $formCouple->handleRequest($request);

        if ($formCouple->isValid()) {
            $em->persist($couple);
            $em->flush();
        }
 
        return $this->render('MyWeddingProfileBundle:Profile:edit_profile.html.twig', array(
            'formCouple' => $formCouple->createView(),

        ));

       
    }


}
